//
//  OverlayView.m
//  MKOverlayTest
//
//  Created by onegray on 3/20/13.
//  Copyright (c) 2013 onegray. All rights reserved.
//

#import "OverlayView.h"
#import "Overlay.h"

@implementation OverlayView
- (void)drawMapRect:(MKMapRect)mapRect zoomScale:(MKZoomScale)zoomScale inContext:(CGContextRef)context
{
	Overlay* overlay = (Overlay*)[self overlay];
	
	CGRect r = [self rectForMapRect:overlay.boundingMapRect];
	
	CGContextSetFillColorWithColor(context, [[UIColor colorWithWhite:0.0 alpha:0.2] CGColor]);
	CGContextFillEllipseInRect(context, r);
	
	CGContextSetStrokeColorWithColor(context, [[UIColor redColor] CGColor]);
	CGContextSetLineWidth(context, 10);
	CGContextStrokeEllipseInRect(context, r);
}
@end
