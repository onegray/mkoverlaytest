//
//  ViewController.m
//  MKOverlayTest
//
//  Created by onegray on 3/20/13.
//  Copyright (c) 2013 onegray. All rights reserved.
//

#import "ViewController.h"
#import "Overlay.h"
#import "OverlayView.h"

@interface ViewController ()
@property (nonatomic, strong) Overlay* overlay;
@property (nonatomic, strong) OverlayView* overlayView;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.overlay = [[Overlay alloc] init];
	
	[self.mapView addOverlay:_overlay];

	MKCoordinateRegion mapRegion;
	mapRegion.center = _overlay.coordinate;
	mapRegion.span = MKCoordinateSpanMake(0.003, 0.003);
	[self.mapView setRegion:mapRegion animated:YES];
}

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(Overlay*)overlay
{
	if(!self.overlayView)
	{
		self.overlayView = [[OverlayView alloc] initWithOverlay:overlay];
	}
	return self.overlayView;
}

	-(IBAction)onUpdateOverlayBtn:(id)sender
	{
		_overlay.boundingMapRect = MKMapRectInset(_overlay.boundingMapRect, -3000, -3000);
		[_overlayView setNeedsDisplayInMapRect:_overlay.boundingMapRect];
	}

@end
