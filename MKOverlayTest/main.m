//
//  main.m
//  MKOverlayTest
//
//  Created by onegray on 3/20/13.
//  Copyright (c) 2013 onegray. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
