//
//  AppDelegate.h
//  MKOverlayTest
//
//  Created by onegray on 3/20/13.
//  Copyright (c) 2013 onegray. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
