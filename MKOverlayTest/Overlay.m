//
//  Overlay.m
//  MKOverlayTest
//
//  Created by onegray on 3/20/13.
//  Copyright (c) 2013 onegray. All rights reserved.
//

#import "Overlay.h"

@implementation Overlay

- (id)init
{
	self = [super init];
	if (self) {
		_coordinate = CLLocationCoordinate2DMake(37.78577, -122.40645);
		_boundingMapRect.origin = MKMapPointForCoordinate(_coordinate);
		_boundingMapRect.size = MKMapSizeMake(200, 200);
	}
	return self;
}

@end
