//
//  ViewController.h
//  MKOverlayTest
//
//  Created by onegray on 3/20/13.
//  Copyright (c) 2013 onegray. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController <MKMapViewDelegate>

@property (nonatomic, strong) IBOutlet MKMapView* mapView;

-(IBAction)onUpdateOverlayBtn:(id)sender;

@end
